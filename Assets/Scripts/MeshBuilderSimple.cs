using UnityEngine;
using Unity.Collections;
using System.Collections.Generic;

static class MeshBuilderSimple
{
    // reusable fields
    static bool isAllocated;
    static List<Vector3> verts;
    static List<Vector2> uvs;
    static List<int> tris;

    public static void Build(NativeSlice<Coin> coins, float size, Mesh mesh)
    {
        if (!isAllocated)
        {
            // Buffer allocation
            verts = new List<Vector3>();
            uvs = new List<Vector2>();
            tris = new List<int>();
            isAllocated = true;
        }

        verts.Clear();
        for (var i = 0; i < coins.Length; i++)
        {
            var p = coins[i].Position;
            verts.Add(new Vector3(p.x - size, p.y - size, 0));
            verts.Add(new Vector3(p.x - size, p.y + size, 0));
            verts.Add(new Vector3(p.x + size, p.y + size, 0));
            verts.Add(new Vector3(p.x + size, p.y - size, 0));
        }

        uvs.Clear();
        var uv0 = new Vector2(0, 0);
        var uv1 = new Vector2(0, 1);
        var uv2 = new Vector2(1, 1);
        var uv3 = new Vector2(1, 0);
        for (var i = 0; i < coins.Length; i++)
        {
            uvs.Add(uv0);
            uvs.Add(uv1);
            uvs.Add(uv2);
            uvs.Add(uv3);
        }

        tris.Clear();
        for (var i = 0; i < coins.Length * 4; i++)
            tris.Add(i);

        mesh.Clear();
        mesh.SetVertices(verts);
        mesh.SetUVs(0, uvs);
        mesh.SetIndices(tris, MeshTopology.Quads, 0);
    }
}
