using Unity.Mathematics;
using UnityEngine;

readonly struct Coin
{
    public float2 Position { get; }
    public float2 Velocity { get; }

    public Coin(float2 position, float2 velocity)
    {
        Position = position;
        Velocity = velocity;
    }

    public static Coin Spawn(float2 position, int seed)
    {
        var r = new RHash((uint)seed);
        var angle = r.Float(math.PI * 2, 0u);
        var speed = r.Float(0.05f, 0.2f, 1u);
        return new Coin(position,
                          math.float2(math.cos(angle),
                                      math.sin(angle)) * speed);
    }

    public Coin NextFrame(float delta)
        => new Coin(Position + Velocity * delta, Velocity);

    /*   {
           return new Coin(Position + Velocity * delta, Velocity);
       }*/
}

readonly struct CoinGroupInfo
{
    public int ActiveCount { get; }
    public int SpawnCount { get; }

    public CoinGroupInfo(int activeCount, int spawnCount)
      => (ActiveCount, SpawnCount) = (activeCount, spawnCount);

    public static CoinGroupInfo InitialState
      => new CoinGroupInfo(0, 0);

    public static CoinGroupInfo
      ChangeActiveCount(in CoinGroupInfo orig, int count)
      => new CoinGroupInfo(count, orig.SpawnCount);

    public static CoinGroupInfo
      AddActiveAndSpawnCount(in CoinGroupInfo orig, int add)
      => new CoinGroupInfo(orig.ActiveCount + add, orig.SpawnCount + add);
}
