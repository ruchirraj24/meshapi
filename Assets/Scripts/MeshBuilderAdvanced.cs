using UnityEngine;
using UnityEngine.Rendering;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

static class MeshBuilderAdvanced
{
    public static void Build(NativeSlice<Coin> coins, float size, Mesh mesh)
    {
        var coinCount = coins.Length;
        var vertexCount = coinCount * 4;

        mesh.Clear();

        // Allocation
        var varray = new NativeArray<CoinQuad>(coinCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
        var iarray = new NativeArray<uint>(vertexCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
        
        // Jobs
        var vertsJob = new VertexConstructionJob(coins, size, varray);
        var trisJob = new IndexConstructionJob(iarray);

        var handle = vertsJob.Schedule(coinCount, 64);
        handle = trisJob.Schedule(vertexCount, 64, handle);

        handle.Complete();

        // Verts
        mesh.SetVertexBufferParams(vertexCount, new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 2), new VertexAttributeDescriptor(VertexAttribute.TexCoord0,VertexAttributeFormat.Float32, 2));
        mesh.SetVertexBufferData(varray, 0, 0, coinCount);

        // Tris
        mesh.SetIndexBufferParams(vertexCount, IndexFormat.UInt32);
        mesh.SetIndexBufferData(iarray, 0, 0, vertexCount);

        // Mesh assignment
        var meshDesc = new SubMeshDescriptor(0, vertexCount, MeshTopology.Quads);
        mesh.SetSubMesh(0, meshDesc, MeshUpdateFlags.DontRecalculateBounds);

        // Cleanup
        varray.Dispose();
        iarray.Dispose();
    }

    readonly struct CoinQuad
    {
        readonly float4 v0, v1, v2, v3;

        public CoinQuad(float2 center, float size)
        {
            v0 = math.float4(center.x - size, center.y + size, 0, 1);
            v1 = math.float4(center.x + size, center.y + size, 1, 1);
            v2 = math.float4(center.x + size, center.y - size, 1, 0);
            v3 = math.float4(center.x - size, center.y - size, 0, 0);
        }
    }

    // Verts
    [BurstCompile]
    struct VertexConstructionJob : IJobParallelFor
    {
        [ReadOnly] NativeSlice<Coin> coins;
        [WriteOnly] NativeArray<CoinQuad> output;
        float _size;

        public VertexConstructionJob(NativeSlice<Coin> coins, float size,
                                     NativeArray<CoinQuad> output)
          => (this.coins, this.output, _size) = (coins, output, size);

        public void Execute(int i)
          => output[i] = new CoinQuad(coins[i].Position, _size);
    }

    // Tris
    [BurstCompile]
    struct IndexConstructionJob : IJobParallelFor
    {
        [WriteOnly] NativeArray<uint> _output;

        public IndexConstructionJob(NativeArray<uint> output)
          => _output = output;

        public void Execute(int i)
          => _output[i] = (uint)i;
    }
}